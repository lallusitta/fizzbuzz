

public class FizzBuzz {
    public static String start(int number) {
        String value = "";
        for(int i =1; i<=number; i++){
            if (((i % 3) == 0) && ((i % 5) == 0)){
                value += "fizzbuzz";
            }
            if (((i % 3) == 0 )&& ((i % 5) != 0)) {
                value += "fizz,";
            }
            if (((i % 3) != 0 )&& ((i % 5) == 0)) {
                value += "buzz,";
            }
            if (((i % 3) != 0 )&& ((i % 5) != 0)) {
                value += ""+i+",";
            }
        }
        return value;
    }
}
